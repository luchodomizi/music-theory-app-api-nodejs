const express = require("express");
const cors = require("cors");
const sgMail = require("@sendgrid/mail");
const axios = require("axios");
require("dotenv").config();

const app = express();

app.use(cors());
app.use(express.json());

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

app.post("/send-email", async (req, res) => {
  const { email, name, message } = req.body;

  const mail = {
    to: ["luchodomizi@gmail.com", email],
    from: {
      name: "Humberto Domizi",
      email: process.env.FROM_EMAIL,
    },
    subject: "Consulta - Music Theory App",
    html: `    <div>
    <h1>Nuevo mensaje de ${name}</h1>
    <p>${message}</p>
    <p>${email}</p>
  </div>`,
  };

  //Validamos si el email es una dirección válida
  const options = {
    method: "GET",
    url: "https://api.zerobounce.net/v2/validate",
    params: {
      api_key: process.env.CHECK_EMAIL_API_KEY,
      email: email,
    },
  };

  try {
    const response = await axios.request(options);

    if (response.data.status === "valid") {
      try {
        await sgMail
          .send(mail)
          .then((response) => console.log("Email enviado"))
          .catch((e) => console.log(e));

        res.status(200).send("Mail Enviado");
      } catch (error) {
        console.error(error);
        res.status(500).send(error.message);
      }
    } else {
      res.status(500).send("Error al validar el mail, ingrese un mail válido");
    }
  } catch (error) {
    console.error(error);
  }
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
