# Music Theory App - Backend - Hackaton SOHO 2023

Api en Nodejs y express para enviar mails de contacto desde Music Theory App utilizando SDK de SendGrid, e integrando la api de ZeroBounce para validar si la dirección email es válida

# Para levantar el proyecto seguir éstos pasos:

- git clone
- npm i
- npm run dev

Dejo el **.env** en el repo para la prueba, luego se eliminará la API KEY y el mail FROM.

El mail se me enviará a mi y una copia al mail que ingresaste en el Frontend. Probablemente llegue a la carpeta de spams.

Recursos:

- NodeJS 18.16.0
- Express
- SDK Sendgrid
- API ZeroBounce (Verifica validez de dirección email)

# Para enviar mails, se deben levantar los 2 proyectos en local.

- Repo del frontend: https://gitlab.com/luchodomizi/music-theory-app-nextjs

Humberto Domizi - https://www.linkedin.com/in/humbertodomizi/
